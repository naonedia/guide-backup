[![By Naonedia](https://img.shields.io/badge/by-Naonedia-blue.svg)](http://naonedia.fr) [![Licence](https://img.shields.io/badge/license-GPL--3.0-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Gitlab](https://img.shields.io/badge/sources-gitlab-green.svg)](https://gitlab.com/naonedia/guide) [![Project Documentation](https://img.shields.io/badge/project-rtfd-green.svg)](https://readthedocs.org/projects/naonedia) [![Documentation Status](http://readthedocs.org/projects/naonedia/badge/?version=latest)](http://naonedia.readthedocs.io) 

# Guide de dépôt

     Copyright (c) 2018-2021 Naonedia http://naonedia.fr/contact

Naonedia propose d'héberger les codes sources des projets IA accompagnés ou développés par le collectif ouvertes en collaboration opensource. 

Retrouvez ici **[la documentation en ligne](https://naonedia.gitlab.io/guide)**.

__Licence__

Ce projet et les codes sources déposées sur le dépôt gitlab.com sous le groupe [Naonedia](https://gitlab.com/naonedia) sont sous licence [GPL-3.0](LICENCE).

[![Licence](https://www.gnu.org/graphics/gplv3-88x31.png)](LICENSE)
